import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConsoleDTO } from '../ConsoleDTO';
import { ConsolePage } from '../ConsolePage';
@Injectable({
  providedIn: 'root',
})
export class ListService {
  constructor(private http: HttpClient) {
    this.http = http;
  }

  private apiUrl = 'http://localhost:8080/api/v1/console';

  getAll(): Observable<ConsolePage> {
    return this.http.get<ConsolePage>(this.apiUrl);
  }
}
