export interface ConsoleDTO {
  id: number;
  name: string;
  releaseYear: number;
  imgUrl: string;
  createDate: string;
  updateDate: string;
}
