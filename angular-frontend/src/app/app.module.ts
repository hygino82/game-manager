import { NgModule } from '@angular/core';
import { BrowserModule, provideClientHydration } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ConsoleListComponent } from './components/console-list/console-list.component';
import { HttpClientModule } from '@angular/common/http';
import { ConsoleInsertFormComponent } from './components/console-insert-form/console-insert-form.component';

@NgModule({
  declarations: [
    AppComponent,
    ConsoleListComponent,
    ConsoleInsertFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule
  ],
  providers: [
    provideClientHydration()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
