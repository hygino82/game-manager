import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsoleInsertFormComponent } from './console-insert-form.component';

describe('ConsoleInsertFormComponent', () => {
  let component: ConsoleInsertFormComponent;
  let fixture: ComponentFixture<ConsoleInsertFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ConsoleInsertFormComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ConsoleInsertFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
