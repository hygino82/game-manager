import { Component, OnInit } from '@angular/core';
import { ConsoleDTO } from '../../ConsoleDTO';
import { ListService } from './../../services/console.service';
import { ConsolePage } from '../../ConsolePage';

@Component({
  selector: 'app-console-list',
  templateUrl: './console-list.component.html',
  styleUrl: './console-list.component.css',
})
export class ConsoleListComponent implements OnInit {
  consoles: ConsoleDTO[] = [];
  consolePage: ConsolePage = {
    content: [],
    first: true,
    last: true,
    size: 1,
    number: 0,
    empty: false,
    numberOfElements: 1,
    totalElements: 1,
    totalPages: 1,
  };

  constructor(private listService: ListService) {
    this.getConsoles();
  }

  getConsoles(): void {
    this.listService
      .getAll()
      .subscribe((page) => (this.consoles = page.content));
  }

  ngOnInit(): void {}
}
