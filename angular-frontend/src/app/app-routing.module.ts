import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConsoleListComponent } from './components/console-list/console-list.component';
import { ConsoleInsertFormComponent } from './components/console-insert-form/console-insert-form.component';

const routes: Routes = [
  {
    path: '',
    component: ConsoleListComponent,
  },
  {
    path: 'console-form',
    component: ConsoleInsertFormComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],

  exports: [RouterModule],
})
export class AppRoutingModule {}
