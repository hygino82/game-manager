import { ConsoleDTO } from './ConsoleDTO';

export interface ConsolePage {
  content: ConsoleDTO[];
  last: boolean;
  totalPages: number;
  totalElements: number;
  size: number;
  number: number;
  first: boolean;
  numberOfElements: number;
  empty: boolean;
}
