import axios from "axios";
import { useEffect, useState } from "react";
import { BiMessageAltAdd } from "react-icons/bi";
import { Console, ConsolePageType } from "../../types/custom-types";
import { BASE_URL } from "../../utils/request";
import "./styles.css";

const ConsoleInsert = () => {
  const [name, setName] = useState<string | null>(null);
  const [releaseYear, setReleaseYear] = useState<number | null>(0);
  const [imgUrl, setImgUrl] = useState<string | null>(null);
  const [pageConsole, setPageConsole] = useState<ConsolePageType>();
  const [atualizar, setAtualizar] = useState<any>('');

  const [platform, setPlatForm] = useState<Console>({
    name: '',
    releaseYear: 0,
    imgUrl: ''
  });

  function limparCampos() {
    setPlatForm({
      name: '',
      releaseYear: 0,
      imgUrl: ''
    });
  }

  function handleSubmit(event: any) {
    event.preventDefault();
    if (platform.id) {
      //console.log(`Modificando objeto com o Id: ${servico.id}`);
      axios.put(`${BASE_URL}/console/${platform.id}`, platform).then((result) => {
        setAtualizar(result);
      });
    } else {
      //console.log(`Registro criado`);
      axios.post(`${BASE_URL}/console`, platform).then((result) => {
        setAtualizar(result);
      });
    }
    limparCampos();
  }

  useEffect(() => {
    buscarTodos();
  }, [atualizar])

  function handleChange(event: any) {
    setPlatForm({ ...platform, [event.target.name]: event.target.value });
    console.log(`Modificando valores`);
  }

  function buscarTodos() {
    axios.get(`${BASE_URL}/console`).then((result) => {
      const valores = result.data;
      setPageConsole(valores);
      setAtualizar(result.data);
    });

  }

  const inserir = () => {
    axios
      .post(`${BASE_URL}/console`, {
        name: name,
        releaseYear: releaseYear,
        imgUrl: imgUrl,
      })
      .then(function (response) {
        console.log(response.data.name);
      })
      .catch(function (error) {
        console.log(error);
      });
    alert("Console adicionado");
  };

  function handleDeletar(id: number | undefined) {
    axios
      .delete(`${BASE_URL}/console/${id}`)
      .then(() => setAtualizar(''));
  }

  return (
    <>
      <div className="container">
        <form>
          <div className="entrada">
            <label>Nome do console</label>
            <input
              type="text"
              placeholder="Nome do console"
              onChange={handleChange}
              value={platform.name || ''}
            />
          </div>
          <div className="entrada">
            <label>Ano de lançamento</label>
            <input
              type="number"
              placeholder="Ano de lançamento"
              onChange={handleChange}
              value={platform.releaseYear || ''}
            />
          </div>
          <div className="entrada">
            <label>Link da imagem</label>
            <input
              type="text"
              placeholder="Link da imagem"
              onChange={handleChange}
              value={platform.imgUrl || ''}
            />
            <button onClick={handleSubmit} className="btn btn-primary">
              <BiMessageAltAdd />
              Adicionar
            </button>
          </div>
        </form>
      </div>

      <div className="saida">
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Id</th>
              <th scope="col">Nome</th>
              <th scope="col">lançamento</th>
              <th scope="col">Link da imagem</th>
              <th scope="col">Ações</th>
            </tr>
          </thead>
          <tbody>
            {pageConsole?.content.map(val => {
              return (
                <tr key={val.id}>
                  <th scope="row">{val.id}</th>
                  <td>{val.name}</td>
                  <td>{val.releaseYear}</td>
                  <td>{val.imgUrl}</td>
                  <td>
                    <button className="btn btn-warning" onClick={() => setPlatForm(platform)}>Editar</button> &nbsp;&nbsp;
                    <button className="btn btn-danger" onClick={() => handleDeletar(val.id)}>Remover</button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default ConsoleInsert;
