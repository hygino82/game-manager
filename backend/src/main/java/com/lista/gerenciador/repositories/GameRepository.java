package com.lista.gerenciador.repositories;


import com.lista.gerenciador.entities.Game;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface GameRepository extends JpaRepository<Game, Long> {

	@Query("SELECT obj FROM Game obj WHERE obj.id = :id")
	Optional<Game> findGameById(Long id);

	@Query("SELECT obj FROM Game obj WHERE obj.console.id = :id") 
    Page<Game> findGamesByConsole(Long id, Pageable pageable);

	//@Query("SELECT obj FROM Game obj WHERE UPPER(obj.personalCode) LIKE UPPER(:code)")
	@Query("SELECT obj FROM Game obj WHERE UPPER(obj.personalCode) LIKE CONCAT('%', UPPER(:code), '%')")
	List<Game> findGamesByCode(String code);
}
