package com.lista.gerenciador.repositories;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.lista.gerenciador.entities.Console;

public interface ConsoleRepository extends JpaRepository<Console, Long> {

	@Query("SELECT obj FROM Console obj WHERE obj.id = :id")
	Optional<Console> findConsoleById(Long id);
}
