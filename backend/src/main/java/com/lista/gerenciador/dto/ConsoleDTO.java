package com.lista.gerenciador.dto;

import com.lista.gerenciador.entities.Console;

import java.util.Date;

public class ConsoleDTO {

	private Long id;

	private String name;
	private Integer releaseYear;
	private Date createDate;
	private Date updateDate;
	private String imgUrl;

	public ConsoleDTO() {
	}

	public ConsoleDTO(Long id, String name, Integer releaseYear) {
		this.id = id;
		this.name = name;
		this.releaseYear = releaseYear;
	}

	public ConsoleDTO(Long id, String name, Integer releaseYear, String imgUrl) {
		this.id = id;
		this.name = name;
		this.releaseYear = releaseYear;
		this.imgUrl = imgUrl;
	}

	public ConsoleDTO(Console entity) {
		id = entity.getId();
		name = entity.getName();
		releaseYear = entity.getReleaseYear();
		imgUrl = entity.getImgUrl();
		createDate = entity.getCreateDate();
		updateDate = entity.getUpdateDate();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getReleaseYear() {
		return releaseYear;
	}

	public void setReleaseYear(Integer releaseYear) {
		this.releaseYear = releaseYear;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
}
