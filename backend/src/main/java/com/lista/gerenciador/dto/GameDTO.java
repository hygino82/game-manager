package com.lista.gerenciador.dto;

import java.util.Date;

import com.lista.gerenciador.entities.Game;

public class GameDTO {
	
    private Long id;
    private String name;
    private Integer releaseYear;
    private String imageUrl;
    private String personalCode;
    private String consoleName;
    
    private Date createDate;
    private Date updateDate;

    public GameDTO() {
    }
    
    public GameDTO(Game entity) {
		id = entity.getId();
		name = entity.getName();
		releaseYear = entity.getReleaseYear();
		imageUrl = entity.getImageUrl();
		personalCode = entity.getPersonalCode();
		consoleName = entity.getConsole().getName();
		createDate = entity.getCreateDate();
		updateDate = entity.getUpdateDate();
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(Integer releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

	public String getConsoleName() {
		return consoleName;
	}

	public void setConsoleName(String consoleName) {
		this.consoleName = consoleName;
	}

    public String getPersonalCode() {
        return personalCode;
    }

    public void setPersonalCode(String personalCode) {
        this.personalCode = personalCode;
    }

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
}
