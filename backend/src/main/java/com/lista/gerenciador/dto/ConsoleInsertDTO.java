package com.lista.gerenciador.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

public class ConsoleInsertDTO {

	@NotEmpty
	private String name;

	@NotNull
	private Integer releaseYear;
	
	private String imgUrl;

	public ConsoleInsertDTO() {
	}

	public ConsoleInsertDTO(String name, @NotNull Integer releaseYear, String imgUrl) {
		this.name = name;
		this.releaseYear = releaseYear;
		this.imgUrl = imgUrl;
	}

	public ConsoleInsertDTO(String name, Integer releaseYear) {
		this.name = name;
		this.releaseYear = releaseYear;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getReleaseYear() {
		return releaseYear;
	}

	public void setReleaseYear(Integer releaseYear) {
		this.releaseYear = releaseYear;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
}
