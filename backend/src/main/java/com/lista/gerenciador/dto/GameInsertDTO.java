package com.lista.gerenciador.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

public class GameInsertDTO {

	@NotEmpty
	private String name;
	
	private Integer releaseYear;
	private String imageUrl;
	private String personalCode;

	@NotNull
	private Long consoleId;

	public GameInsertDTO() {
	}

	public GameInsertDTO(String name, Integer releaseYear, String imageUrl, String personalCode, Long consoleId) {
		this.name = name;
		this.releaseYear = releaseYear;
		this.imageUrl = imageUrl;
		this.personalCode = personalCode;
		this.consoleId = consoleId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getReleaseYear() {
		return releaseYear;
	}

	public void setReleaseYear(Integer releaseYear) {
		this.releaseYear = releaseYear;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Long getConsoleId() {
		return consoleId;
	}

	public void setConsoleId(Long consoleId) {
		this.consoleId = consoleId;
	}

	public String getPersonalCode() {
		return personalCode;
	}

	public void setPersonalCode(String personalCode) {
		this.personalCode = personalCode;
	}
}
