package com.lista.gerenciador.controllers;

import java.net.URI;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.lista.gerenciador.dto.GameDTO;
import com.lista.gerenciador.dto.GameInsertDTO;
import com.lista.gerenciador.services.GameService;

import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.websocket.server.PathParam;

@RestController
@RequestMapping("/api/v1/game")
@CrossOrigin("*")
public class GameController {

	private final GameService gameService;

	public GameController(GameService gameService) {
		this.gameService = gameService;
	}

	@GetMapping
	@Tag(name = "Listar todos os Games")
	public ResponseEntity<Page<GameDTO>> findAll(Pageable pageable) {
		Page<GameDTO> page = gameService.findAll(pageable);

		return ResponseEntity.ok().body(page);
	}

	@GetMapping("/{id}")
	@Tag(name = "Buscar Game por id")
	public ResponseEntity<GameDTO> findGameById(@PathVariable("id") Long id) {
		GameDTO dto = gameService.findById(id);

		return ResponseEntity.ok().body(dto);
	}

	@PostMapping
	@Tag(name = "Inserir Game")
	public ResponseEntity<GameDTO> insertGame(@RequestBody GameInsertDTO dto) {
		GameDTO newDto = gameService.insert(dto);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(newDto.getId()).toUri();

		return ResponseEntity.created(uri).body(newDto);
	}

	@PutMapping(value = "/{id}")
	@Tag(name = "Atualizar Game")
	public ResponseEntity<GameDTO> update(@PathVariable("id") Long id, @RequestBody GameInsertDTO dto) {
		GameDTO newDto = new GameDTO();
		newDto = gameService.update(id, dto);

		return ResponseEntity.ok().body(newDto);
	}

	@DeleteMapping(value = "/{id}")
	@Tag(name = "Remover Game pelo Id")
	public ResponseEntity<GameDTO> delete(@PathVariable("id") Long id) {
		gameService.delete(id);

		return ResponseEntity.noContent().build();
	}

	@GetMapping("/console/{id}")
	@Tag(name = "Buscar jogos pelo console")
	public ResponseEntity<Page<GameDTO>> findGamesByConsole(@PathVariable("id") Long id, Pageable pageable) {
		Page<GameDTO> page = gameService.findGamesByConsole(id, pageable);

		return ResponseEntity.ok().body(page);
	}

	@GetMapping("/code")
	@Tag(name = "Buscar jogos pelo código pessoal")
	public ResponseEntity<List<GameDTO>> findGamesByCode(@PathParam(value = "code") String code) {
		List<GameDTO> list = gameService.findGamesByCode(code);

		return ResponseEntity.ok(list);
	}
}
