package com.lista.gerenciador.controllers;

import java.net.URI;
import java.util.List;

import org.springframework.data.domain.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.lista.gerenciador.dto.ConsoleDTO;
import com.lista.gerenciador.dto.ConsoleInsertDTO;
import com.lista.gerenciador.services.ConsoleService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/console")
@CrossOrigin("*")
public class ConsoleController {

    private final ConsoleService consoleService;

    public ConsoleController(ConsoleService consoleService) {
        this.consoleService = consoleService;
    }


    @GetMapping
    @Tag(name = "Listar todos os consoles por página")
    public ResponseEntity<Page<ConsoleDTO>> findAll(Pageable pageable) {
        Page<ConsoleDTO> page = consoleService.findAll(pageable);

        return ResponseEntity.ok().body(page);
    }

    @GetMapping("/lista")
    @Tag(name = "Mostrar todos os consoles em uma lista")
    public ResponseEntity<List<ConsoleDTO>> listAllConsoles() {
        List<ConsoleDTO> list = consoleService.listAllConsoles();

        return ResponseEntity.ok().body(list);
    }

    @GetMapping("/{id}")
    @Tag(name = "Buscar console por id")
    public ResponseEntity<ConsoleDTO> findConsoleById(@PathVariable("id") Long id) {
        ConsoleDTO dto = consoleService.findConsoleById(id);

        return ResponseEntity.ok().body(dto);
    }

    @PostMapping
    @Tag(name = "Inserir Console")
    public ResponseEntity<ConsoleDTO> insertConsole(@RequestBody @Valid ConsoleInsertDTO dto) {
        ConsoleDTO newDto = consoleService.insert(dto);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(newDto.getId()).toUri();

        return ResponseEntity.created(uri).body(newDto);
    }

    @PutMapping(value = "/{id}")
    @Tag(name = "Atualizar Console")
    public ResponseEntity<ConsoleDTO> update(@PathVariable("id") Long id, @RequestBody @Valid ConsoleInsertDTO dto) {
        ConsoleDTO newDto = consoleService.update(id, dto);

        return ResponseEntity.ok().body(newDto);
    }

    @DeleteMapping(value = "/{id}")
    @Tag(name = "Remover console pelo Id")
    public ResponseEntity<ConsoleDTO> delete(@PathVariable("id") Long id) {
        consoleService.delete(id);

        return ResponseEntity.noContent().build();
    }
}
