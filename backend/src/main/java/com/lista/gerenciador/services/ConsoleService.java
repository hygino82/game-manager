package com.lista.gerenciador.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lista.gerenciador.dto.ConsoleDTO;
import com.lista.gerenciador.dto.ConsoleInsertDTO;
import com.lista.gerenciador.entities.Console;
import com.lista.gerenciador.repositories.ConsoleRepository;
import com.lista.gerenciador.services.exception.DatabaseException;
import com.lista.gerenciador.services.exception.ResourceNotFoundException;

import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;

@Service
public class ConsoleService {

    private final ConsoleRepository consoleRepository;

    public ConsoleService(ConsoleRepository consoleRepository) {
        this.consoleRepository = consoleRepository;
    }

    @Transactional(readOnly = true)
    public Page<ConsoleDTO> findAll(Pageable pageable) {
        Page<Console> page = consoleRepository.findAll(pageable);

        return page.map(x -> new ConsoleDTO(x));
    }

    @Transactional(readOnly = true)
    public List<ConsoleDTO> listAllConsoles() {
        List<Console> list = consoleRepository.findAll();

        return list.stream().map(x -> new ConsoleDTO(x)).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public ConsoleDTO findConsoleById(Long id) {
        Optional<Console> obj = consoleRepository.findById(id);
        Console entity = obj.orElseThrow(() -> new ResourceNotFoundException("Entity not found"));

        return new ConsoleDTO(entity);
    }

    @Transactional
    public ConsoleDTO insert(@Valid ConsoleInsertDTO dto) {
        Console entity = new Console();
        entity.setName(dto.getName());
        entity.setReleaseYear(dto.getReleaseYear());
        entity.setCreateDate(new Date());
        entity.setImgUrl(dto.getImgUrl());
        entity = consoleRepository.save(entity);

        return new ConsoleDTO(entity);
    }

    @Transactional
    public ConsoleDTO update(Long id, @Valid ConsoleInsertDTO dto) {
        try {
            Console entity = consoleRepository.getReferenceById(id);
            entity.setName(dto.getName());
            entity.setReleaseYear(dto.getReleaseYear());
            entity.setUpdateDate(new Date());
            entity.setImgUrl(dto.getImgUrl());
            entity = consoleRepository.save(entity);
            return new ConsoleDTO(entity);
        } catch (EntityNotFoundException e) {
            throw new ResourceNotFoundException("Id not found " + id);
        }
    }

    public void delete(Long id) {
        try {
            consoleRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ResourceNotFoundException("Id not found " + id);
        } catch (DataIntegrityViolationException e) {
            throw new DatabaseException("Integrity violation");
        }
    }
}
