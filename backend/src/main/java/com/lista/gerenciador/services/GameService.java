package com.lista.gerenciador.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lista.gerenciador.dto.GameDTO;
import com.lista.gerenciador.dto.GameInsertDTO;
import com.lista.gerenciador.entities.Console;
import com.lista.gerenciador.entities.Game;
import com.lista.gerenciador.repositories.ConsoleRepository;
import com.lista.gerenciador.repositories.GameRepository;
import com.lista.gerenciador.services.exception.DatabaseException;
import com.lista.gerenciador.services.exception.ResourceNotFoundException;

import jakarta.persistence.EntityNotFoundException;

@Service
public class GameService {

	private final GameRepository gameRepository;
	private final ConsoleRepository consoleRepository;

	public GameService( GameRepository gameRepository, ConsoleRepository consoleRepository) {
		this.gameRepository = gameRepository;
		this.consoleRepository = consoleRepository;
	}

	@Transactional(readOnly = true)
	public Page<GameDTO> findAll(Pageable pageable) {
		Page<Game> page = gameRepository.findAll(pageable);
		Page<GameDTO> pageDto = page.map(game -> new GameDTO(game));

		return pageDto;
	}

	@Transactional(readOnly = true)
	public GameDTO findById(Long id) {
		Optional<Game> obj = gameRepository.findById(id);
		Game entity = obj.orElseThrow(() -> new ResourceNotFoundException("Entity not found"));

		return new GameDTO(entity);
	}

	@Transactional
	public GameDTO insert(GameInsertDTO dto) {
		Game entity = dtoToEntity(dto);
		entity.setCreateDate(new Date());

		entity = gameRepository.save(entity);

		return new GameDTO(entity);
	}

	@Transactional
	public GameDTO update(Long id, GameInsertDTO dto) {
		try {
			Optional<Console> consoleOptional = consoleRepository.findById(dto.getConsoleId());
			Game entity = gameRepository.getReferenceById(id);
			Console consoleEntity = consoleOptional
					.orElseThrow(() -> new ResourceNotFoundException("Console Entity not found"));
			entity.setConsole(consoleEntity);
			entity.setName(dto.getName());
			entity.setImageUrl(dto.getImageUrl());
			entity.setReleaseYear(dto.getReleaseYear());
			entity.setUpdateDate(new Date());

			entity = gameRepository.save(entity);

			return new GameDTO(entity);
		} catch (EntityNotFoundException e) {
			throw new ResourceNotFoundException("Id not found " + id);
		}
	}

	public void delete(Long id) {
		try {
			gameRepository.deleteById(id);
		} catch (EmptyResultDataAccessException e) {
			throw new ResourceNotFoundException("Id not found " + id);
		} catch (DataIntegrityViolationException e) {
			throw new DatabaseException("Integrity violation");
		}
	}

	protected Game dtoToEntity(GameInsertDTO dto) {
		Game entity = new Game();
		entity.setImageUrl(dto.getImageUrl());
		entity.setReleaseYear(dto.getReleaseYear());
		entity.setImageUrl(dto.getImageUrl());
		entity.setName(dto.getName());
		entity.setPersonalCode(dto.getPersonalCode());
		Optional<Console> consoleOptional = consoleRepository.findById(dto.getConsoleId());
		Console console = consoleOptional.orElseThrow(() -> new ResourceNotFoundException("Console Entity not found"));
		entity.setConsole(console);

		return entity;
	}

	public Page<GameDTO> findGamesByConsole(Long id, Pageable pageable) {
		Page<Game> page = gameRepository.findGamesByConsole(id, pageable);
		Page<GameDTO> pageDto = page.map(game -> new GameDTO(game));

		return pageDto;
	}

	public List<GameDTO> findGamesByCode(String code) {
		List<Game> list = gameRepository.findGamesByCode(code);
		List<GameDTO> listDto = list.stream().map(game -> new GameDTO(game)).collect(Collectors.toList());

		return listDto;
	}
}
