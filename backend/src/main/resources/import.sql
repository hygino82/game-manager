INSERT INTO tb_console(name, release_year, create_date, img_url) VALUES('Nintendo', 1983, NOW(), 'https://upload.wikimedia.org/wikipedia/commons/0/06/Nintendo-Famicom-Console-Set-FL.jpg');
INSERT INTO tb_console(name, release_year, create_date, img_url) VALUES('Super Nintendo', 1990, NOW(),'https://pt.wikipedia.org/wiki/Super_Nintendo_Entertainment_System#/media/Ficheiro:SNES-Mod1-Console-Set.jpg');
INSERT INTO tb_console(name, release_year, create_date, img_url) VALUES('Nintendo 64', 1996, NOW(), 'https://pt.wikipedia.org/wiki/Nintendo_64#/media/Ficheiro:N64-Console-Set.png');
INSERT INTO tb_console(name, release_year, create_date, img_url) VALUES('Mega Drive', 1988, NOW(), 'https://pt.wikipedia.org/wiki/Mega_Drive#/media/Ficheiro:Sega-Mega-Drive-JP-Mk1-Console-Set.jpg');
INSERT INTO tb_console(name, release_year, create_date, img_url) VALUES('Playstation 1', 1994, NOW(), 'https://pt.wikipedia.org/wiki/PlayStation_(console)#/media/Ficheiro:PSX-Console-wController.png');
INSERT INTO tb_console(name, release_year, create_date, img_url) VALUES('Playstation 2', 2000, NOW(), 'https://pt.wikipedia.org/wiki/PlayStation_2#/media/Ficheiro:PS2-Versions.png');
INSERT INTO tb_console(name, release_year, create_date, img_url) VALUES('Playstation 3', 2006, NOW(), 'https://pt.wikipedia.org/wiki/PlayStation_3#/media/Ficheiro:Sony-PlayStation-3-CECHA01-wController-L.jpg');
INSERT INTO tb_console(name, release_year, create_date, img_url) VALUES('Playstation 4', 2013, NOW(), 'https://pt.wikipedia.org/wiki/PlayStation_4#/media/Ficheiro:Sony-PlayStation-4-PS4-wDualShock-4.jpg');
INSERT INTO tb_console(name, release_year, create_date, img_url) VALUES('Playstation 5', 2020, NOW(), 'https://pt.wikipedia.org/wiki/PlayStation_5#/media/Ficheiro:PlayStation_5_and_DualSense_with_transparent_background.png');
INSERT INTO tb_console(name, release_year, create_date, img_url) VALUES('XBOX 360', 2005, NOW(), 'https://pt.wikipedia.org/wiki/Xbox_360#/media/Ficheiro:Xbox-360-Pro-wController.jpg');
INSERT INTO tb_console(name, release_year, create_date, img_url) VALUES('XBOX ONE', 2013, NOW(), 'https://pt.wikipedia.org/wiki/Xbox_One#/media/Ficheiro:Microsoft-Xbox-One-Console-Set-wKinect.jpg');

INSERT INTO tb_game(console_id, name, release_year, image_url, create_date) VALUES (4, 'Comix Zone', 1995, 'https://upload.wikimedia.org/wikipedia/en/0/0e/Comix_Zone_Coverart.png', NOW());
INSERT INTO tb_game(console_id, name, release_year, image_url, create_date) VALUES (4, 'Sonic the Hedgehog 3', 1994, 'https://upload.wikimedia.org/wikipedia/pt/6/6c/Sonic_the_Hedgehog_3_capa.png', NOW());
INSERT INTO tb_game(console_id, name, release_year, image_url, create_date) VALUES (7, 'Gran Turismo 5', 2010, 'https://upload.wikimedia.org/wikipedia/en/8/8b/GT5-boxart-final-EU.jpg', NOW());
INSERT INTO tb_game(console_id, name, release_year, image_url, create_date) VALUES (7, 'Gran Turismo 6', 2013, 'https://upload.wikimedia.org/wikipedia/pt/b/bc/Gran_Turismo_6_capa.png', NOW());
INSERT INTO tb_game(console_id, name, release_year, image_url, create_date) VALUES (6, 'Black', 2006, 'https://upload.wikimedia.org/wikipedia/pt/a/ad/Black_cover_art.jpg', NOW());
INSERT INTO tb_game(console_id, name, release_year, image_url, create_date) VALUES (6, 'Blood Roar 3', 2000, 'https://upload.wikimedia.org/wikipedia/en/c/c8/Bloody_Roar_3_Coverart.png', NOW());
INSERT INTO tb_game(console_id, name, release_year, image_url, create_date) VALUES (2, 'Super Mario World', 1990, 'https://upload.wikimedia.org/wikipedia/pt/0/06/Super-Mario-World.jpg', NOW());
