package com.lista.gerenciador.services;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.lista.gerenciador.dto.ConsoleDTO;
import com.lista.gerenciador.dto.ConsoleInsertDTO;
import com.lista.gerenciador.entities.Console;
import com.lista.gerenciador.repositories.ConsoleRepository;
import com.lista.gerenciador.util.Factory;

@SpringBootTest
class ConsoleServiceTest {

	@InjectMocks
	ConsoleService service;

	private ConsoleInsertDTO dto = Factory.consoleInsertDTO();

	@Mock
	private ConsoleRepository repository;

	private List<Console> consoles;

	@BeforeEach
	void setUp() {
		// Inserindo consoles a lista
		consoles = new ArrayList<>();
		consoles.add(new Console(1L, "Super Nintendo", "nes.jpg", 1983));
		consoles.add(new Console(2L, "Super Nintendo", "snes.jpg", 1990));
		consoles.add(new Console(3L, "Mega Drive", "mega-drive.jpg", 1988));
	}

	@Test
	@DisplayName("Teste de inserção de um novo console")
	void insertTest() {

		// Criação do objeto Console esperado após a inserção
		Console console = new Console(dto);
		Console consoleComId = new Console(dto);
		consoleComId.setId(1L);

		// Configuração do comportamento do mock do consoleRepository
		when(repository.save(console)).thenReturn(consoleComId);

		// Chamada do método insert do service
		ConsoleDTO result = service.insert(dto);

		// Verificação dos resultados esperados
		Assertions.assertNotNull(result);
		Assertions.assertNotNull(result.getId());
		Assertions.assertEquals(dto.getReleaseYear(), result.getReleaseYear());
		Assertions.assertEquals(dto.getImgUrl(), result.getImgUrl());
		Assertions.assertEquals(dto.getName(), result.getName());

		// Verificação se o método save do consoleRepository foi chamado
		verify(repository, times(1)).save(any(Console.class));
	}

	@Test
	@DisplayName("Teste de atualização de um novo console")
	void updateConsole() {

		// Criação do DTO de atualização fictício para o teste
		long id = 1L;
		// Date updateDate = Date.parse("2010-01-01T12:00:00+01:00");
		ConsoleInsertDTO dto = new ConsoleInsertDTO("Updated Console", 2022, "https://example.com/updated-console.jpg");

		// Criação do objeto Console fictício para simular o objeto existente no
		// repositório
		Console updateConsole = new Console(id, "Updated Console", "https://example.com/updated-console.jpg", 2022);
		updateConsole.setUpdateDate(new Date());
		Console existingConsole = new Console(id, "Original Console", "https://example.com/original-console.jpg", 2021);
		existingConsole.setCreateDate(new Date());
		existingConsole.setImgUrl("https://example.com/original-console.jpg");
		existingConsole.setUpdateDate(new Date());

		// Configuração do comportamento do mock do consoleRepository
		when(repository.getReferenceById(id)).thenReturn(existingConsole);
		when(repository.save(any(Console.class))).thenReturn(updateConsole);

		// Chamada do método update do service
		ConsoleDTO result = service.update(id, dto);

		// Verificação dos resultados esperados
		Assertions.assertEquals(dto.getName(), result.getName());
		Assertions.assertEquals(dto.getReleaseYear(), result.getReleaseYear());
		Assertions.assertEquals(dto.getImgUrl(), result.getImgUrl());
		Assertions.assertNotNull(result);

		// Verificação se o método getReferenceById do consoleRepository foi chamado
		verify(repository, times(1)).getReferenceById(id);

		// Verificação se o método save do consoleRepository foi chamado
		verify(repository, times(1)).save(existingConsole);
	}

	@Test
	@DisplayName("Buscando consoles de forma paginada")
	void testFindAll() {
		// Arrange
		Pageable pageable = Pageable.unpaged();
		Page<Console> page = new PageImpl<>(consoles);

		when(repository.findAll(pageable)).thenReturn(page);

		// Act
		Page<ConsoleDTO> result = service.findAll(pageable);

		// Assert
		assertEquals(consoles.size(), result.getContent().size());
		assertEquals(consoles.get(0).getName(), result.getContent().get(0).getName());
		assertEquals(consoles.get(1).getName(), result.getContent().get(1).getName());
		assertEquals(consoles.get(2).getName(), result.getContent().get(2).getName());
	}

	@Test
	@DisplayName("Listando todos os consoles")
	void testListAllConsoles() {
		// Arrange
		when(repository.findAll()).thenReturn(consoles);

		// Act
		List<ConsoleDTO> result = service.listAllConsoles();

		// Assert
		assertEquals(consoles.size(), result.size());
		assertEquals(consoles.get(0).getName(), result.get(0).getName());
		assertEquals(consoles.get(1).getName(), result.get(1).getName());
		assertEquals(consoles.get(2).getName(), result.get(2).getName());
		assertEquals(consoles.get(2).getImgUrl(), result.get(2).getImgUrl());
	}

	@Test
	@DisplayName("Buscando console com Id válido")
	void testFindConsoleById() {
		// Arrange
		Long id = 1L;
		Console console = consoles.get(0);

		when(repository.findById(id)).thenReturn(Optional.of(console));

		// Act
		ConsoleDTO result = service.findConsoleById(id);

		// Assert
		assertEquals(console.getName(), result.getName());
		assertEquals(console.getImgUrl(), result.getImgUrl());
	}

	@Test
	@DisplayName("Removendop console pelo seu Id")
	void testDelete() {
		// Arrange
		Long id = 1L;

		// No need to mock consoleRepository.deleteById(id) as it's a void method

		// Act & Assert
		assertDoesNotThrow(() -> service.delete(id));
	}
}
