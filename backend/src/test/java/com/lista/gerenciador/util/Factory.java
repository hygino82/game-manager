package com.lista.gerenciador.util;

import com.lista.gerenciador.dto.ConsoleInsertDTO;

public class Factory {
    private static String name = "Atari 2600";
    private static int year = 1982;
    private static String url = "https://pt.wikipedia.org/wiki/Atari_2600#/media/Ficheiro:Atari-2600-Wood-4Sw-Set.png";

    // Criação do DTO de inserção fictício para o teste
    public static ConsoleInsertDTO consoleInsertDTO() {
        return new ConsoleInsertDTO(name, year, url);
    }
}
