import 'bootstrap/dist/css/bootstrap.css';
import './App.css';
import NavBar from './components/NavBar';
import ConsoleInsertForm from './pages/ConsoleInsertForm';
import ConsoleList from './pages/ConsoleList';

function App() {
  return (
    <div className="container">
      <NavBar />
      <ConsoleInsertForm />
      <ConsoleList />
    </div>
  );
}

export default App;
