export type Console = {
    id?: number;
    name: String;
    releaseYear?: number;
};

export type Game = {
    id?: number;
    name: string;
    releaseYear: number;
    consoleName?: string;
    consoleId?: number;
    imageUrl: string;
    personalCode?: string;
};

export type ConsolePageType = {
    content: Console[];
    last: boolean;
    totalPages: number;
    totalElements: number;
    size: number;
    number: number;
    first: boolean;
    numberOfElements: number;
    empty: boolean;
}

export type GamePageType = {
    content: Game[];
    last: boolean;
    totalPages: number;
    totalElements: number;
    size: number;
    number: number;
    first: boolean;
    numberOfElements: number;
    empty: boolean;
}