import { Console } from "../../types/custom-types";

type Props = {
    consoles: Console[];
}

function ConsoleSelector({ consoles }: Props) {
   
    return (
        <select className="form-select form-select-lg mb-3" aria-label=".form-select-lg example">
            {
                consoles.map(x => {
                    return <option value={x.id}>{x.name}</option>;
                })
            }
        </select>
    );
}

export default ConsoleSelector;