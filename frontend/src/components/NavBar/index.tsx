function NavBar() {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item active">
                        <a className="nav-link" href="#">Inserir console</a>
                    </li>
                    <li className="nav-item active">
                        <a className="nav-link" href="#">Inserir jogo</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">Listar consoles</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">Listar jogos</a>
                    </li>
                </ul>
            </div>
        </nav>
    );
}

export default NavBar;