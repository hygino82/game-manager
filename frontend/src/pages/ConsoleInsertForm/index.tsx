import axios from "axios";
import { useState } from "react";
import { BASE_URL } from "../../utils/request";
import { Console } from "../../types/custom-types";
import "./style.css";

function ConsoleInsertForm() {

    const [name, setName] = useState<String>("");
    const [releaseYear, setReleaseYear] = useState<Number>(0);

    function getName(e: any) {
        e.preventDefault();
        setName(e.target.value);
    }

    function getYear(e: any) {
        e.preventDefault();
        setReleaseYear(Number(e.target.value));
    }


    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        try {
            console.log('Testando a ação do botão submit');
            console.log(`Você informou nome:${name}, ano:${releaseYear}`);
            const response = await axios.post<Console>(`${BASE_URL}/console`, {
                name: name,
                releaseYear: Number(releaseYear)
            });
            console.log(response.data);
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <div className="container" id="inserir-console">
            <form onSubmit={handleSubmit}>
                <label htmlFor="txtNome">Nome do console</label>
                <input
                    type="text"
                    name="txtNome"
                    id="txtNome"
                    onChange={(e) => getName(e)}
                />
                <br />
                <label htmlFor="txtAno">Ano de lançamento</label>
                <input
                    type="number"
                    name="txtAno"
                    id="txtAno"
                    onChange={(e) => getYear(e)}
                />
                <br />
                <button type="submit" className="btn btn-primary" >Adicionar</button>
            </form>
        </div>
    );
}

export default ConsoleInsertForm;