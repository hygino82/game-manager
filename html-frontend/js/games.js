let btnListar = document.getElementById('btn-listar');
let tabela = document.getElementById('tabela');

const BASE_URL = 'http://localhost:8080/api/v1';

function listarDados(e) {
    e.preventDefault();
    tabela.innerHTML = '';
    fetch(`${BASE_URL}/game`)
        .then(response => response.json())
        .then(data => {
            data.content.forEach(obj => {
                tabela.innerHTML += `
            <tr>
                <th scope="row">${obj.id}</th>
                <td>${obj.name}</td>
                <td>${obj.consoleName}</td>
                <td>${obj.releaseYear}</td>
                <td>${(obj.createDate == null) ? '----' : obj.createDate}</td>
                <td>${(obj.updateDate == null) ? '----' : obj.updateDate}</td>
                <td> <i class="fa-solid fa-pen"></i></td>
            


            </tr>
            `;
            })
        })
        .catch(err => console.log(err));
}

btnListar.addEventListener('click', listarDados);